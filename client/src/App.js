import React from 'react';
import AutoComplete from './components/AutoComplete';

const App = () => (
  <div className="container">
    <AutoComplete />
  </div>
);

export default App;
