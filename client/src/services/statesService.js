import api from './api';

export default {
  /**
   * @description Return all the states that matches the term.
   * @param term string
   */
  search: (term = '') => api.get(`/states?term=${term}`),
};
