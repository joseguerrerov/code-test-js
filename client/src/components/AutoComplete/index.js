import React, { useState } from 'react';
import PropTypes from 'prop-types';
import statesService from '../../services/statesService';

const AutoComplete = ({ onSelect, onClear }) => {
  const [searchValue, setSearchValue] = useState('');
  const [matchingStates, setMatchingStates] = useState([]);
  const [selectedValue, setSelectedValue] = useState(-1);

  const existResults = matchingStates.length > 0;

  /**
   * @description Async method to search state
   */
  const searchState = async (term) => {
    const { data: responseData } = await statesService.search(term);
    const { data } = responseData;
    setMatchingStates(data.slice(0, 5));
  };

  /**
   * @description Clear autocomplete state
   */
  const clear = () => {
    setSearchValue('');
    setSelectedValue(-1);
    setMatchingStates([]);
    onClear();
  };

  /**
   * @description Handle input onChange event
   * @param value string
   */
  const handleValueChange = (value) => {
    setSearchValue(value);
    setSelectedValue(-1);
    if (value.length < 2) {
      setSelectedValue(-1);
      return setMatchingStates([]);
    }
    return searchState(value);
  };

  /**
   * @description Select state and clean matchingStates Array
   */
  const selectState = () => {
    setSearchValue(matchingStates[selectedValue].name);
    setMatchingStates([]);
    onSelect(matchingStates[selectedValue].name);
  };
  /**
   * @description Update selectedValue and searchValue
   * @param newSelectedValue string
   */
  const updateSelectedValue = (newSelectedValue) => {
    setSelectedValue(newSelectedValue);
    setSearchValue(matchingStates[newSelectedValue].name);
  };

  const handleKeyDown = ({ keyCode, key }) => {
    const upArrowKey = 38;
    const downArrowKey = 40;
    // Return if there are not values to iterate
    if (!existResults) return;
    // Select state when Enter is pressed and exists a valid selectedValue
    if (selectedValue !== -1 && key === 'Enter') selectState();

    if (keyCode === downArrowKey && selectedValue === matchingStates.length - 1) {
      updateSelectedValue(0);
    } else if (keyCode === downArrowKey && selectedValue < matchingStates.length) {
      updateSelectedValue(selectedValue + 1);
    } else if (keyCode === upArrowKey && selectedValue === 0) {
      updateSelectedValue(matchingStates.length - 1);
    } else if (keyCode === upArrowKey && selectedValue >= 0) {
      updateSelectedValue(selectedValue - 1);
    }
  };

  /**
   * @description Render list of matching states
   */
  const renderMatchingStates = () => (
    <div>
      {matchingStates.map(({ name, abbreviation }, index) => (
        <button
          type="button"
          className="result"
          style={{ ...selectedValue === index ? { background: '#88DDF6' } : {} }}
          key={abbreviation}
          onMouseEnter={() => setSelectedValue(index)}
          onMouseLeave={() => setSelectedValue(-1)}
          onClick={selectState}
        >
          <p>{name}</p>
        </button>
      ))}
    </div>
  );

  return (
    <div className="autocomplete">
      <div style={{ ...existResults ? { borderBottom: '1px solid #999999' } : {} }}>
        <input
          type="text"
          name="state name"
          aria-required="true"
          aria-describedby="search state"
          value={searchValue}
          onChange={({ target: { value } }) => handleValueChange(value)}
          onKeyDown={handleKeyDown}
          placeholder="Search states"
        />
        {(existResults || selectedValue !== -1) && (
          <button type="button" onClick={clear} className="close-button">
            x
          </button>
        )}
      </div>
      {existResults && renderMatchingStates()}
    </div>
  );
};

AutoComplete.propTypes = {
  onClear: PropTypes.func,
  onSelect: PropTypes.func,
};

AutoComplete.defaultProps = {
  onClear: () => {},
  onSelect: () => {},
};

export default AutoComplete;
