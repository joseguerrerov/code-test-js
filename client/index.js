import React from 'react';
import ReactDOM from 'react-dom';
import './sass/app.scss';
import App from './src/App';

// Element to render React app
const wrapper = document.getElementById('app');
export default wrapper ? ReactDOM.render(<App />, wrapper) : false;
