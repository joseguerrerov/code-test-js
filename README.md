# Verndale JS Code Test


[![CircleCI](https://circleci.com/gh/joseguerrerov/code-test-js/tree/master.svg?style=svg&circle-token=ddf1cc99d08b66760a9b4569a788c0f8e3df11fd)](https://circleci.com/gh/joseguerrerov/code-test-js/tree/master)

### Description

Simple autocomplete component to search states from the U.S.
It is built with React and implements React Hooks in order to
manage _state_ in functional components in a much declarative way.

### Build steps

* `npm install` or `yarn`
* `npm run start` o `yarn start` "_I update the version of webpack :)_"
* open http://localhost:3000/

### Lint

* `npm run lint` o `yarn lint`
