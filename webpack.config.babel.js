import webpack from 'webpack';
import WebpackShellPlugin from 'webpack-shell-plugin';

export default {
  entry:  './client/index.js',
  output: {
    path: __dirname + '/dist',
    filename: '[name]-bundle.js'
  },
  watch: true,
  module: {
    rules: [
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: {
          loader: "babel-loader"
        }
      },
      {
        test: /\.scss$/,
        exclude: '/node_modules/',
        loaders: ['style-loader', 'css-loader', 'sass-loader']
      }
    ]
  },
  plugins: [
    new WebpackShellPlugin({
      onBuildStart: ['echo "Starting"'],
      onBuildEnd: ['babel-node ./server/server.js']
    })
  ],
  node: {
    fs: 'empty'
  },
  resolve: {
    extensions: ['*', '.js', '.scss']
  }
}
